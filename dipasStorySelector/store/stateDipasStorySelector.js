/**
 * QuickResponseCode tool state definition.
 * @typedef {Object} DipasStorySelectorState
 * @property {String} id Id of the DipasStorySelector tool.
 * @property {Boolean} active If true, DipasStorySelector will be rendered.
 * @property {String} name Displayed as the title.
 * @property {String} icon Icon next to the title.
 * @property {Boolean} renderToWindow If true, tool is rendered in a window, else in the sidebar.
 * @property {Boolean} resizableWindow Specifies whether the window should be resizable.
 * @property {Boolean} deactivateGFI Specifies whether the gfi should be disabled when this tool is opened.
 */
const state = {
    id: "dipasStorySelector",
    active: false,
    name: "translate#additional:modules.tools.dipasStorySelector.title",
    icon: "bi-bookshelf",
    renderToWindow: true,
    resizableWindow: true,
    deactivateGFI: true,
    storyIndexURL: null
};

export default state;

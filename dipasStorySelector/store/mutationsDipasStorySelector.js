import {generateSimpleMutations} from "../../../../src/app-store/utils/generators";
import stateDipasStorySelector from "./stateDipasStorySelector";

const mutations = {
    ...generateSimpleMutations(stateDipasStorySelector)
};

export default mutations;

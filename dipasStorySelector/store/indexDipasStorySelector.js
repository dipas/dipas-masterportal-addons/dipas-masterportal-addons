import getters from "./gettersDipasStorySelector";
import mutations from "./mutationsDipasStorySelector";
import state from "./stateDipasStorySelector";

export default {
    namespaced: true,
    state,
    getters,
    mutations
};

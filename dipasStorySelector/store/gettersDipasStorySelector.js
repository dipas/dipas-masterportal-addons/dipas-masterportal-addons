import {generateSimpleGetters} from "../../../../src/app-store/utils/generators";
import stateDipasStorySelector from "./stateDipasStorySelector";

const getters = {
    ...generateSimpleGetters(stateDipasStorySelector)
};

export default getters;

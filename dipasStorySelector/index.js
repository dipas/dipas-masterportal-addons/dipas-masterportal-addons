import DipasStorySelector from "./components/DipasStorySelector.vue";
import DipasStorySelectorStore from "./store/indexDipasStorySelector";
import deLocale from "./locales/de/additional.json";
import enLocale from "./locales/en/additional.json";

export default {
    component: DipasStorySelector,
    store: DipasStorySelectorStore,
    locales: {
        de: deLocale,
        en: enLocale
    }
};

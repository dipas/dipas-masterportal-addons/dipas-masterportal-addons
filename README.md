# DIPAS Masterportal AddOns

The DIPAS Masterportal AddOns repository contains Masterportal Addons devoloped for DIPAS.
Some Addons such as the StoryTellingTool can also be used in other projects outside of DIPAS.

## Setup
In order to use any Addons in the Masterportal the repository containing the Addons needs to be cloned into the addons folder inside the Masterportal setup.
As DIPAS uses Addons from another Masterportal Addons repository as well as the DIPAS Addons, the DIPAS Masterportal Addons repository cannot be directly cloned into the addons folder inside the Masterportal structure.
The DIPAS Masterportal Addons repository needs therefore be cloned into another folder inside the addons folder.
Like any other Addon, the Addons of the DIPAS Masterportal Addons repository need to be added to the addonsConf.json in the addons folder.
The DIPAS Masterportal Addons need to include the path to the respective Addons.
If the DIPAS Masterportal Addons repository has been cloned into the "dipasAddons" folder the configuration of the Addons in the addonsConf.json would look as follows:

"dataNarrator": {
    "path": "dipasAddons/dataNarrator",
    "type": "tool"
  },
  "dipasStorySelector": {
    "path": "dipasAddons/dipasStorySelector",
    "type": "tool"
  }



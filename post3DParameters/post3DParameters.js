import mapCollection from "../../../src/core/maps/mapCollection";
import store from "../../../src/app-store";

Backbone.Events.listenTo(Radio.channel("MapView"), {
    "changedCenter": function () {
        if (store.getters["Maps/mode"] === "3D") {
            const camera = mapCollection.getMap("3D").getCesiumScene().camera;

            Radio.trigger("RemoteInterface", "postMessage", {
                "cameraPosition": toDegrees(camera.position),
                "cameraHeading": camera.heading,
                "cameraPitch": camera.pitch,
                "cameraRoll": camera.roll
            });
        }
    }
});

Backbone.Events.listenTo(Radio.channel("Map"), {
    "change": function () {
        if (store.getters["Maps/mode"] === "3D") {

            const camera = mapCollection.getMap("3D").getCesiumScene().camera;

            Radio.trigger("RemoteInterface", "postMessage", {
                "cameraPosition": toDegrees(camera.position),
                "cameraHeading": camera.heading,
                "cameraPitch": camera.pitch,
                "cameraRoll": camera.roll
            });

        }
    }
});


/**
 * Transform caratesian coordinates to degree
 * @param {Object} cartesian3Pos a position defined by longitude, latitude, and height.
 * @returns {number} value in the resulting object will be in degrees
 */
function toDegrees (cartesian3Pos) {
    const pos = Cesium.Cartographic.fromCartesian(cartesian3Pos);

    return [pos.longitude / Math.PI * 180, pos.latitude / Math.PI * 180, pos.height];
}

